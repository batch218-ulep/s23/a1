// console.log("Mabuhay");

// Item 1
let trainer = {

	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(pokemonIndex){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);

console.log("Result from dot notation: ");
console.log(trainer.name);

console.log("Result from square bracket notation: ");
console.log(trainer['pokemon']);

console.log('Result of talk method');
trainer.talk();

console.log('');

// Item 2

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);

		if (target.health <= 0) {
			this.faint(target);
		}
	};
	this.faint = function(target) {
		console.log(target.name + " fainted");
	};
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

console.log('');

// Item 3
geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);
